import Vue from "vue";
import App from "./App.vue";

import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faCommentDots,
  faQuestionCircle,
  faCheckCircle,
  faImage,
  faComments,
} from "@fortawesome/free-regular-svg-icons";
import {
  faEllipsisH,
  faLayerGroup,
  faPlusCircle,
  faTrash,
  faPlus,
  faTimes,
  faChevronLeft,
  faChevronRight
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(
  faCommentDots,
  faQuestionCircle,
  faCheckCircle,
  faImage,
  faEllipsisH,
  faLayerGroup,
  faComments,
  faPlusCircle,
  faTrash,
  faPlus,
  faTimes,
  faChevronLeft,
  faChevronRight
);

Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount("#app");
